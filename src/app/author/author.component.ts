import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  authors: any[];
  _from = 0
  loading: boolean
  total: any
  _to = 12
  constructor(private es:ElasticsearchService) { }

  ngOnInit() {
    this.es.size = this._from
    this.es.getAuthors(environment.index, environment.type)
    .then(res => {
      this.authors = Array.from (new Set(res.hits.hits.map(movie => movie._source.author)))
    })
  }

  authorMovies(author){

     this.es.filterByAuthors(environment.index,environment.type, this.es.size, this._to, author).
     then(res => {
       console.log(res)
       this.es.setBooks(res.hits.hits)
       this.total = res.hits.total
       this.loading = false
     })
  }

}
