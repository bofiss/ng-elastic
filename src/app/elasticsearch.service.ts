import { Injectable } from '@angular/core'
import * as elasticsearch from 'elasticsearch'
import * as env from '../environments/environment'
import { environment } from '../environments/environment.prod'
import { Book } from './models/book.model'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

@Injectable()
export class ElasticsearchService {
  private client = elasticsearch.client
  books: Book[]  = []
  size: any
  key: string
  private range1Source = new BehaviorSubject(1)
  private range2Source = new BehaviorSubject(5)
  private booksSource = new BehaviorSubject<any[]>([])

  castBooks = this.booksSource.asObservable()

  castRange1 = this.range1Source.asObservable()
  castRange2 = this.range2Source.asObservable()


  queryAllDocs = {
    "query" : {
      "range" : {
            "ratings" : {
              "gte" : 1,
              "lte" : 5,
              "boost" : 2.0
            }
        }
    },
    "sort": [
      { '_uid' : { 'order' : 'desc'}}
    ]
  }

  constructor() {
    if(!this.client) {
      this.connect()
    }
   }

  private connect() {
    this.client = new elasticsearch.Client({
      host: 'localhost:9200',
      user: env.environment.user,
      password: env.environment.password
    })
  }

  private getQuery(key:string, param1:any, param2:any,){
    switch (key) {
      case "ratings":
        return {
          "query" : {
            "range" : {
                  "ratings" : {
                    "gte" : param1,
                    "lte" : param2,
                    "boost" : 2.0
                  }
              }
          },
          "sort": [
            { '_uid' : { 'order' : 'desc'}}
          ]
        }
      case "year" :
      return {
        "query" : {
          "range" : {
              "year" : {
                "gte" : param1,
                "lte" : param2,
                "boost" : 2.0
              }
          }
        },
        "sort": [
          { '_uid' : { 'order' : 'desc'}}
        ]
      }
      default:
        break;
    }

  }

  // get all docs
  getAllDocs(_index, _type, _from, _to, key, range1,range2): any {
 
    return this.client.search({
       index: _index,
       type: _type,
       'from' : _from,
       'size': _to,
      body: this.getQuery(key, range1, range2),
      filter_path: ['hits']  
    })
  }

  // get single doc by id

  getDoc(_index, _type, _id): any {
    return this.client.search({
      
      index: _index,
      type: _type,
      body: this.queryAllDocs,
      filter_path: ['hits']  
    })
  }
  
  updateRange(newRange: number, key:string) {

    switch (key) {
      case 'min':
        this.range1Source.next(newRange)
        break

      case 'max':
        this.range2Source.next(newRange)
        break

      default:
        break;
    }
    
  }

  movieSearch(_index, _type, _from, _to, _field, _queryText) {
    return this.client.search({
      index: _index,
       type: _type,
       'from' : _from,
       'size': _to,
      body: {
        "query": {
          "match_phrase_prefix": {
             [_field]: _queryText
          }
        }
      },
      _source: ['author', 'title']
    })
 }

 setBooks(books:Array<any>){
     this.booksSource.next(books)
 }

 filterByRating(_index, _type, _from, _to, rating) {
    return this.client.search({
      index: _index,
      type: _type,
      from : _from,
      size: _to,
      body: {
        "query": {
          "bool": { 
            "filter": [ 
              { "term":  { "ratings": rating }}
            ]
          }
        }
      }
    })
  }
  getAuthors(_index, _type){
     return this.client.search({
      index: _index,
      type: _type,
      size:10000,
      body: {
        "aggs": {
          "movies": {
            "terms": {
              "field": "{author}"
            }
          }
        }
      }
     })
  }

  filterByAuthors(_index, _type, _from, _to, author) {
    return this.client.search({
      index: _index,
      type: _type,
      from : _from,
      size: _to,
      body: {
        "query": {
          "bool": { 
            "must": [
              { "match": { "author":   author }}
            ]
          }
        }
      }
    })
  }


}



