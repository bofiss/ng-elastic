import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-ratingview',
  templateUrl: './ratingview.component.html',
  styleUrls: ['./ratingview.component.css']
})

export class RatingviewComponent implements  OnInit {

  loading: boolean
  total: any
  _from = 0
  _to = 12
  isActive = false
  ratings= [1,2,3,4,5]

  constructor(private es:ElasticsearchService) { }

  ngOnInit() {
    this.es.size = this._from
  }


  getBooksByrating(rating) {

    this.ratings.filter(i => i !== rating)
     .map(i => {
          let res = document.getElementById(i.toString())
          res.querySelector('input').checked = false
      })
     
    this.es.filterByRating(environment.index, environment.type, this.es.size, this._to, rating)
     .then((res) => {
         this.es.setBooks(res.hits.hits)
         this.total = res.hits.total
         this.loading = false;
     })
  }
   
}

  


