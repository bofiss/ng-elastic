import { Component, OnInit, Input, Output, EventEmitter, ViewChild,
  AfterViewInit,
  ElementRef } from '@angular/core'

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.css']
})
export class RateComponent implements OnInit {
  
  @Input() rate:number

   isActive = false
  @Output() goRating = new EventEmitter<number>()
  constructor() { }

  ngOnInit() {
  }

  onRating($event){

     this.goRating.emit($event)


     
  }


 

}
