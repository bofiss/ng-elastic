import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RangsliderComponent } from './rangslider.component';

describe('RangsliderComponent', () => {
  let component: RangsliderComponent;
  let fixture: ComponentFixture<RangsliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RangsliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangsliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
