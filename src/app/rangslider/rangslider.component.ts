import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'


@Component({
  selector: 'app-rangslider',
  templateUrl: './rangslider.component.html',
  styleUrls: ['./rangslider.component.css']
})

export class RangsliderComponent implements OnInit {

  @Input() min:number 
  @Input() max:number 
  @Input() step:number

  @Output() getMin = new EventEmitter<number>()
  @Output() getMax = new EventEmitter<number>()

  constructor() { }

  ngOnInit() {
  }

  stepUp($event) {
    this.getMin.emit($event.target.value)
  }
  stepDown($event) {
    this.getMax.emit($event.target.value)
  }





}
