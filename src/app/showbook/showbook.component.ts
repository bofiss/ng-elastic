import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service'
import { Book } from '../models/book.model';
import { environment } from '../../environments/environment'

@Component({
  selector: 'app-showbook',
  templateUrl: './showbook.component.html',
  styleUrls: ['./showbook.component.css']
})
export class ShowbookComponent implements OnInit {

  books: Book[]
  loading = false
  total = 0
  page = 1
  limit = 20
  _from = 0
  _to = 12
  _range1: number
  _range2: number

  constructor(private es:ElasticsearchService) { }

  ngOnInit() {
   this.es.size = this._from
   this.es.castRange1.subscribe(range => this._range1 = range)
   this.es.castRange2.subscribe(range => this._range2 = range)
   this.getBooks(this._from, "ratings", this._range1,this._range2)
   
  }

  getBooks(size, range1, range2, key ): void {
    this.loading = true
    this.es.getAllDocs(environment.index, environment.type, size, this._to, key, this._range1, this._range2).then((res) => 
    {
         this.es.setBooks(res.hits.hits)
         this.es.castBooks.subscribe(books => this.books = books)
         this.total = res.hits.total
         this.loading = false;
    })
    
  }

  goToPage(n: number,): void {
    this.page = n
    this.es.size = 10 * (n-1)
    this.getBooks(this.es.size,  this.es.key, this._range1, this._range2)
  }

  onNext(): void {
    this.page++;
    this.es.size +=10
    this.getBooks(this.es.size,  this.es.key, this._range1, this._range2)
  }

  onPrev(): void {
    this.page--
    this.es.size   -=10
    this.getBooks(this.es.size,  this.es.key, this._range1, this._range2)
  }


}
