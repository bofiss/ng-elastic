import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  @Input() rating: number
  constructor() { }

  ngOnInit() {
  }
   
  getArrayNumber(n) {
    return Array.from(new Array(n), (x,i) => i+1)
  }

}
