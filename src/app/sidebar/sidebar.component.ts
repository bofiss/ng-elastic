import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  
  loading: boolean;
  total: number;
  step = 1
  _from = 0
  _to = 12
  _range1 = 1900
  _range2 = 2018
  constructor(private es:ElasticsearchService) { }

  ngOnInit() {
    this.es.size = this._from
  }

  onMax($event) {
    this.es.updateRange($event, 'max')
    this.es.castRange2.subscribe(range => this._range2 = range)
    this.es.getAllDocs(environment.index, environment.type, this.es.size, this._to, "year",  this._range1, this._range2 ).then((res) => 
    {
         this.es.setBooks(res.hits.hits)
         this.total = res.hits.total
         this.loading = false;
    })
  }

  onMin($event) {
    this.es.updateRange($event, 'min')
    this.es.castRange1.subscribe(range => this._range1 = range)
    this.es.getAllDocs(environment.index, environment.type,  this.es.size, this._to, "year", this._range1, this._range1).then((res) => 
    {
         this.es.setBooks(res.hits.hits)
         this.total = res.hits.total
         this.loading = false;
    })
    
  }



}
