export interface Book {
   id: string;
   author:string;
   title: string;
   publishers: string;
   image_thumbnail_url: string;
   image_medium_url: string;
   image_large_url: string;
   ratings: number;

}