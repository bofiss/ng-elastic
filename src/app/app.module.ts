import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BookComponent } from './book/book.component';
import { ShowbookComponent } from './showbook/showbook.component';
import { PaginationComponent } from './pagination/pagination.component';
import { ElasticsearchService } from './elasticsearch.service';
import { RatingComponent } from './rating/rating.component';
import { RangsliderComponent } from './rangslider/rangslider.component';

import { RatingviewComponent } from './ratingview/ratingview.component';
import { RateComponent } from './ratingview/rate/rate.component';
import { AuthorComponent } from './author/author.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    BookComponent,
    ShowbookComponent,
    PaginationComponent,
    RatingComponent,
    RangsliderComponent,

    RatingviewComponent,

    RateComponent,

    AuthorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ElasticsearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
