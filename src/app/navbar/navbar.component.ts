import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  _to = 12
  queryText:string
  lastKeypress = 0
  _range1;
  _range2;
  constructor(private es:ElasticsearchService) { 
    this.queryText = ''
  }

  ngOnInit() {
    this.es.size = 0
  }

  search($event) {
    if ($event.timeStamp - this.lastKeypress > 100) {
      
        this.queryText = $event.target.value
        this.es.movieSearch( environment.index, environment.type, this.es.size, this._to, 'author', this.queryText)
        .then(res => {
          
          this.es.setBooks(res.hits.hits)
          this.es.castBooks.subscribe(books => console.log(books))
        }, error => {
          console.error(error)
        }).then(() => {
          console.log('Search Completed!')
      })
    }
    this.lastKeypress = $event.timeStamp

  }
  

}
