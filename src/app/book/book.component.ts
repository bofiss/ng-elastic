import { Component, OnInit, Input } from '@angular/core';
import { Book } from '../models/book.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  
  @Input() book: Book
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }
  sanitizeUrl(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url)
  }


}
